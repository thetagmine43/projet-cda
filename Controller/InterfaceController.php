<?php

Interface InterfaceController
{
    public function show();

    public function update();

    public function delete();

    public function add();
}