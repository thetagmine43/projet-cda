<?php

include_once 'Controller/InterfaceController.php';
include_once 'Repository/UserRepository.php';

class UserController implements InterfaceController
{

    private UserRepository $userRepository;

    public function __construct()
    {
        $this->userRepository = new UserRepository();
    }

    public function show()
    {
        $data = $this->userRepository->findAllUsers();
        echo json_encode($data);
    }

    public function update()
    {
        //recupere le body de la request
        $dataUser = json_decode(file_get_contents('php://input'), true);

        $user = new User();
        $user->setId($_GET['id']);
        $user->setEmail($dataUser['email']);
        $user->setPassword($dataUser['password']);
        $user->setBirthDate(new \DateTime($dataUser['birthDate']));

        $this->userRepository->updateUser($user);
    }

    public function delete()
    {
        $this->userRepository->deleteUser($_GET['id']);
    }

    public function add()
    {
        //recupere le body de la request
        $dataUser = json_decode(file_get_contents('php://input'), true);

        $user = new User();
        $user->setEmail($dataUser['email']);
        $user->setPassword($dataUser['password']);
        $user->setBirthDate(new \DateTime($dataUser['birthDate']));

        $this->userRepository->addUser($user);
    }
}