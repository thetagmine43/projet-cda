<?php

include_once 'Repository/dbConnection.php';

class PostRepository extends dbConnection
{
    public function findAllPosts()
    {
        $pdo = $this->getPdo();
        $sql = $pdo->prepare('SELECT * FROM Post');
        $sql->execute();

        return $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    public function addPost(Post $post)
    {
        $pdo = $this->getPdo();
        $request = $pdo->prepare('INSERT INTO Post(post_date, content, id_user, id_topic) VALUES (:postDate, :content, :idUser, :idTopic)');

        $request->execute(array(
            'postDate' => $post->getPostDate()->format('Y-m-d'),
            'content' => $post->getContent(),
            'idUser' => $post->getUser()->getId(),
            'idTopic' => $post->getTopic()->getId()
        ));
    }

    public function updatePost(Post $post){
        $pdo = $this->getPdo();
        $request = $pdo->prepare('UPDATE Post SET post_date=:postDate, content=:content, id_user=:idUser, id_topic=:idTopic WHERE id=:id');

        $request->execute(array(
            'id' => $post->getId(),
            'postDate' => $post->getPostDate()->format('Y-m-d'),
            'content' => $post->getContent(),
            'idUser' => $post->getUser()->getId(),
            'idTopic' =>$post->getTopic()->getId()
        ));
    }

    public function deletePost(int $postId)
    {
        $pdo = $this->getPdo();
        $sql = $pdo->prepare('DELETE FROM Post WHERE id=:id');
        $sql->execute(array(
            'id' => $postId
        ));
    }
}