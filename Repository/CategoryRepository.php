<?php

include_once 'Repository/dbConnection.php';
include_once 'Entity/Category.php';

class CategoryRepository extends dbConnection
{
    public function findAllCategory()
    {
        $pdo = $this->getPdo();
        $sql = $pdo->prepare('SELECT * FROM Category');
        $sql->execute();

        return $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findById(int $idCategory): Category
    {

        $pdo = $this->getPdo();
        $request = $pdo->prepare('SELECT id, label FROM Category WHERE id=:id');
        $request->execute(array("id"=> $idCategory));
        $dataUsers = $request->fetch(PDO::FETCH_ASSOC);

        $category = new Category();
        $category->setId($dataUsers['id']);
        $category->setLabel($dataUsers['label']);

        return $category;

    }

    public function addCategory(Category $category)
    {
        $pdo = $this->getPdo();
        $sql = $pdo->prepare('INSERT INTO Category VALUES (:id, :label)');
        $sql->execute(array(
            'id' => 0,
            'label' => $category->getLabel()
        ));
    }

    public function updateCategory(Category $category)
    {
        $pdo = $this->getPdo();
        $sql = $pdo->prepare('UPDATE Category SET label=:label WHERE id=:id');
        $sql->execute(array(
            'label' => $category->getLabel(),
            'id' => $category->getId()
        ));
    }

    public function deleteCategory(int $categoryId)
    {
        $pdo = $this->getPdo();
        $sql = $pdo->prepare('DELETE FROM Category WHERE id=:id');
        $sql->execute(array(
            'id' => $categoryId
        ));
    }
}