<?php

include_once 'Controller/InterfaceController.php';
include_once 'Repository/PostRepository.php';
include_once 'Entity/Post.php';

class PostController implements InterfaceController
{
    private PostRepository $postRepository;
    private UserRepository $userRepository;
    private TopicRepository $topicRepository;

    public function __construct()
    {
        $this->postRepository = new PostRepository();
        $this->userRepository = new UserRepository();
        $this->topicRepository = new TopicRepository();
    }

    public function show()
    {
        // TODO: Implement show() method.
    }

    public function update(){

        $dataPost = json_decode(file_get_contents('php://input'), true);

        $post = new Post();

        $post->setId($_GET['id']);
        $post->setPostDate(new \DateTime ($dataPost['post_date']));
        $post->setContent($dataPost['content']);
        $post->setUser($this->userRepository->findById($dataPost['id_user']));
        $post->setTopic($this->topicRepository->findById($dataPost['id_topic']));

        $this->postRepository->updatePost($post);
    }

    public function delete()
    {
        $this->postRepository->deletePost($_GET['id']);
    }

    public function add()
    {
        $dataPost = json_decode(file_get_contents('php://input'), true);

        $post = new Post();

        $post->setPostDate(new \DateTime($dataPost['postDate']));
        $post->setContent($dataPost['content']);
        $post->setUser($this->userRepository->findById($dataPost['id_user']));
        $post->setTopic($this->topicRepository->findById($dataPost['id_topic']));

        $this->postRepository->addPost($post);
    }
}