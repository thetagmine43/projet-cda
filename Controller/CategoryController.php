<?php

include_once 'Controller/InterfaceController.php';
include_once 'Repository/CategoryRepository.php';

class CategoryController implements InterfaceController
{
    private CategoryRepository $categoryRepository;

    public function __construct()
    {
        $this->categoryRepository = new CategoryRepository();
    }

    public function show()
    {
        $data = $this->categoryRepository->findAllCategory();
        echo json_encode($data);
    }

    public function update()
    {
        //recupere le body de la request
        $dataCateg = json_decode(file_get_contents('php://input'), true);

        $category = new Category();
        $category->setId($_GET['id']);
        $category->setLabel($dataCateg['label']);

        $this->categoryRepository->updateCategory($category);
    }

    public function delete()
    {
        $this->categoryRepository->deleteCategory($_GET['id']);
    }

    public function add()
    {
        //recupere le body de la request
        $dataCateg = json_decode(file_get_contents('php://input'), true);

        $category = new Category();
        $category->setLabel($dataCateg['label']);

        $this->categoryRepository->addCategory($category);
    }
}