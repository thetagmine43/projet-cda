<?php

include_once 'Repository/dbConnection.php';
include_once 'Entity/User.php';

class UserRepository extends dbConnection
{
    public function findAllUsers()
    {
        $pdo = $this->getPdo();
        $sql = $pdo->prepare('SELECT * FROM USER');
        $sql->execute();

        return $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findById(int $idUser): User
    {

        $pdo = $this->getPdo();
        $request = $pdo->prepare('SELECT id, email, password, birth_date FROM User WHERE id=:id');
        $request->execute(array("id"=> $idUser));
        $dataUsers = $request->fetch(PDO::FETCH_ASSOC);

        $user = new User();
        $user->setId($dataUsers['id']);
        $user->setEmail($dataUsers['email']);
        $user->setPassword($dataUsers['password']);
        $user->setBirthDate(new \DateTime ($dataUsers['birth_date']));

        return $user;

    }

    public function addUser(User $user)
    {
       $pdo = $this->getPdo();
       $sql = $pdo->prepare('INSERT INTO User VALUES (:id, :email, :password, :birthDate)');
       $sql->execute(array(
           'id' => 0,
           'email' => $user->getEmail(),
           'password' => $user->getPassword(),
           'birthDate' => $user->getBirthDate()->format('Y-m-d')
       ));
    }

    public function updateUser(User $user)
    {
        $pdo = $this->getPdo();
        $sql = $pdo->prepare('UPDATE User SET email=:email, password=:password, birth_date=:birthDate WHERE id=:id');
        $sql->execute(array(
            'email' => $user->getEmail(),
            'password' => $user->getPassword(),
            'birthDate' => $user->getBirthDate()->format('Y-m-d'),
            'id' => $user->getId()
        ));
    }

    public function deleteUser(int $userId)
    {
        $pdo = $this->getPdo();
        $sql = $pdo->prepare('DELETE FROM User WHERE id=:id');
        $sql->execute(array(
            'id' => $userId
        ));
    }
}