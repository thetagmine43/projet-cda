<?php

include_once 'Controller/InterfaceController.php';
include_once 'Repository/TopicRepository.php';
include_once 'Entity/Topic.php';

class TopicController implements InterfaceController
{

    private TopicRepository $topicRepository;
    private CategoryRepository $categoryRepository;
    private UserRepository $userRepository;

    public function __construct()
    {
        $this->topicRepository = new TopicRepository();
        $this->categoryRepository = new CategoryRepository();
        $this->userRepository = new UserRepository();
    }

    public function show()
    {
        $data = $this->topicRepository->findAllTopics();
        echo json_encode($data);
    }

    public function update(){

        $dataTopic = json_decode(file_get_contents('php://input'), true);

        $topic = new Topic();

        $topic->setId($_GET['id']);
        $topic->setTitle($dataTopic['title']);
        $topic->setUser($this->userRepository->findById($dataTopic['id_user']));
        $topic->setCategory($this->categoryRepository->findById($dataTopic['id_category']));

        $this->topicRepository->updateTopic($topic);
    }

    public function delete()
    {
        $this->topicRepository->deleteTopic($_GET['id']);
    }

    public function add(){

        $dataTopic = json_decode(file_get_contents('php://input'), true);

        $topic = new Topic();

        $topic->setTitle($dataTopic['title']);
        $topic->setUser($this->userRepository->findById($dataTopic['id_user']));
        $topic->setCategory($this->categoryRepository->findById($dataTopic['id_category']));

        $this->topicRepository->addTopic($topic);

    }
}