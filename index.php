<?php

include_once 'Controller/UserController.php';
include_once 'Controller/CategoryController.php';
include_once 'Controller/TopicController.php';
include_once 'Controller/PostController.php';

if (isset($_GET['action']) && isset($_GET['controller']))
{
    $controller = new $_GET['controller'];
    $action = $_GET['action'];
    $controller->$action();
}