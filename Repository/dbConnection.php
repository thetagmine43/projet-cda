<?php

class dbConnection
{
    private PDO $pdo;

    public function __construct(){
        $dbname = "projet_cda";
        $dsn = "mysql:host=localhost;dbname=".$dbname;
        $user = "allan";
        $passwd = "coub43700";

        try
        {
            $this->pdo = new PDO($dsn, $user, $passwd);
        }
        catch(Exception $e)
        {
            die('Erreur : '.$e->getMessage());
        }
    }

    /**
     * @return PDO
     */
    protected function getPdo(): PDO
    {
        return $this->pdo;
    }
}