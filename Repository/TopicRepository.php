<?php

include_once 'Repository/dbConnection.php';
include_once 'Entity/Topic.php';

class TopicRepository extends dbConnection
{
    public function findAllTopics()
    {
        $pdo = $this->getPdo();
        $sql = $pdo->prepare('SELECT * FROM Topic INNER JOIN Category on Topic.id_category = Category.id INNER JOIN User on Topic.id_user = User.id');
        $sql->execute();

        return $sql->fetchAll(PDO::FETCH_ASSOC);
    }

    public function findById(int $idTopic): Topic
    {
        $pdo = $this->getPdo();
        $request = $pdo->prepare('SELECT id, title, id_user, id_category FROM Topic WHERE id=:id');
        $request->execute(array("id"=> $idTopic));
        $dataUsers = $request->fetch(PDO::FETCH_ASSOC);

        $userRepository = new UserRepository();
        $categoryRepository = new CategoryRepository();


        $topic = new Topic();
        $topic->setId($dataUsers['id']);
        $topic->setTitle($dataUsers['title']);
        $topic->setUser($userRepository->findById($dataUsers['id_user']));
        $topic->setCategory($categoryRepository->findById($dataUsers['id_category']));

        return $topic;
    }

    public function addTopic(Topic $topic)
    {
        $pdo = $this->getPdo();
        $request = $pdo->prepare('INSERT INTO Topic(title, id_user, id_category) VALUES (:title,:idUser,:idCategory)');

        $request->execute(array(
            'title' => $topic->getTitle(),
            'idUser' =>$topic->getUser()->getId(),
            'idCategory' =>$topic->getCategory()->getId()

        ));
    }

    public function updateTopic(Topic $topic){
        $pdo = $this->getPdo();
        $request = $pdo->prepare('UPDATE Topic SET title=:title, id_user=:idUser, id_category=:idCategory WHERE id=:id');

        $request->execute(array(
            'id' => $topic->getId(),
            'title' => $topic->getTitle(),
            'idUser' => $topic->getUser()->getId(),
            'idCategory' =>$topic->getCategory()->getId()

        ));
    }

    public function deleteTopic(int $topicId)
    {
        $pdo = $this->getPdo();
        $sql = $pdo->prepare('DELETE FROM Topic WHERE id=:id');
        $sql->execute(array(
            'id' => $topicId
        ));
    }
}